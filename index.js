const app = require("express")();
const Vonage = require("@vonage/server-sdk");

app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/nexmo/sendsms/", (req, res) => {
  console.log("executing nexmo/sendsms/");
  const from = "Test";
  const to = "+639150657902";
  const text = "A text message sent using the Vonage SMS API";

  const vonage = new Vonage({
    apiKey: "ad159183",
    apiSecret: "iQVWIvW0mqA1vwxj",
  });

  vonage.message.sendSms(from, to, text, (err, responseData) => {
    if (err) {
      console.log(err);
    } else {
      if (responseData.messages[0]["status"] === "0") {
        res.send("Message sent successfully.");
      } else {
        res
          .status(500)
          .send(
            `Message failed with error: ${responseData.messages[0]["error-text"]}`
          );
      }
    }
  });
});

app.listen(3000, () => console.log("listening to port 3000..."));
